<?php
/**
 * Front Page Template
 *
 * @package DCBA
 * @subpackage FrontPage
 * @since DCBA 0.2
 */
get_header();
$idyllic_settings = idyllic_get_theme_options(); ?>
<main id="main" class="site-main">
<?php
/********************************************************************/
	do_action('idyllic_display_front_page_features');
	do_action('hokbay_display_about_us');
	do_action('idyllic_display_fact_figure_box');
	do_action('idyllic_display_portfolio_box');
	do_action('hokbay_display_latest_news');
	do_action('idyllic_display_team_member');
	do_action('hokbay_display_events');
	do_action('hokbay_display_membership');
	the_content();

	if(class_exists('Idyllic_Plus_Features')){
		do_action('idyllic_client_box');
	} ?>
</main><!-- end #main -->
<?php
get_footer();