<?php
/**
 * @package    DCBA
 * @subpackage FrontPage
 * @since      0.1
*/

$style_sheet_dir = get_stylesheet_directory();

require_once $style_sheet_dir . '/inc/security.php';
require_once $style_sheet_dir . '/inc/branding.php';
require_once $style_sheet_dir . '/inc/about.php';
require_once $style_sheet_dir . '/inc/latest-news.php';
require_once $style_sheet_dir . '/inc/events.php';
require_once $style_sheet_dir . '/inc/membership-sponsorship.php';
require_once $style_sheet_dir . '/inc/front-page-sponsor-logos.php';
require_once $style_sheet_dir . '/inc/user-profile-fields.php';
require_once $style_sheet_dir . '/inc/fix-event-tickets-select2.php';
require_once $style_sheet_dir . '/inc/woocommerce.php';

add_action('wp_loaded', 'child_replace_parent_function');
add_post_type_support( 'page', 'excerpt' );
add_action( 'wp_enqueue_scripts', 'idyllic_enqueue_styles' );
add_filter('get_custom_logo','hokbay_transparent_logo_on_home');
add_filter( 'excerpt_length', 'wpdocs_custom_excerpt_length', 999 );

function child_replace_parent_function() {
remove_action( 'idyllic_client_box', 'idyllic_client_box_details' );
}

function idyllic_enqueue_styles() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
}

function mms_current_page_url() {
    $pageURL = 'http';
    if( isset($_SERVER["HTTPS"]) ) {
        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
    }
    $pageURL .= "://";
    if ($_SERVER["SERVER_PORT"] != "80") {
        $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
    } else {
        $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
    }
    return $pageURL;
}

function hokbay_transparent_logo_on_home($html) {

    if (is_home() | is_front_page())
        $html = preg_replace('/<img(.*?)\/>/', '<img src="https://dcba.com/wp-content/uploads/2018/02/logo-white-1.png" class="custom-logo" alt="" itemprop="logo" />', $html);

    return $html;
}

function wpdocs_custom_excerpt_length( $length ) {
    return 50;
}

/**** EVENT TICKET ****/

// Disable QR Check in.
/*
if ( class_exists( 'Tribe__Tickets_Plus__Main' ) ) { 
    remove_action( 'tribe_tickets_ticket_email_ticket_bottom', array( Tribe__Tickets_Plus__Main::instance()->qr(), 'inject_qr' ) );
}*/