<?php
/**
 * Custom fileds on user registrations
 * extension to Woocommerce
 *
 * @package DCBA
 * @subpackage Users
 * @since 0.3
 * 
 */
add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields' );

function custom_override_default_address_fields( $address_fields ) {
     $address_fields['company']['required'] = true;
     $address_fields['company']['placeholder'] = 'Company Name or Individual';

     return $address_fields;
}

// Add title to billing fields
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
function custom_override_checkout_fields( $fields ) {
     $fields['billing']['billing_title'] = array(
        'label'     	=> __('Title', 'woocommerce'),
    	'placeholder'   => _x('Title', 'placeholder', 'woocommerce'),
    	'required'  	=> false,
    	'class'     	=> array('form-row-wide'),
    	'clear'     	=> true
     );

     return $fields;
}

add_action( 'woocommerce_admin_order_data_after_shipping_address', 'custom_checkout_field_display_admin_order_meta', 10, 1 );
function custom_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Title').':</strong> ' . get_post_meta( $order->get_id(), '_billing_title', true ) . '</p>';
}


// Reorder fields
add_filter("woocommerce_checkout_fields", "order_fields");
function order_fields($fields) {

    $order = array(
        "billing_first_name", 
        "billing_last_name",
        "billing_title", 
        "billing_company", 
        "billing_address_1", 
        "billing_address_2", 
        "billing_city",
        "billing_state",
        "billing_postcode", 
        "billing_country", 
        "billing_email", 
        "billing_phone"
    );
    foreach($order as $field)
    {
        $ordered_fields[$field] = $fields["billing"][$field];
    }

    $fields["billing"] = $ordered_fields;
    return $fields;

}