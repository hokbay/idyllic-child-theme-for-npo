<?php
/**
 * Display events on Front Page
 *
 * @package    DCBA
 * @subpackage FrontPage
 * @version    1.0
 */

add_action('hokbay_display_events','hokbay_events');
function hokbay_events(){
	$idyllic_settings = idyllic_get_theme_options();
	?>

	<!-- Events Box ============================================= -->
	<div class="about-box">
		<div class="about-box-bg">
			<div class="wrap">
				<div class="box-header">
					<h2 class="box-title freesia-animation zoomIn" data-wow-delay="0.3s"><?php echo esc_html("Events");?></h2>
				</div><!-- end .box-header -->
				<!-- Events list ============================================= -->
				<div class="column clearfix">fdsfgsdf
					<?php echo apply_filters( 'the_content', '[tribe_events view="list"]'); ?>
				</div><!-- end .colum -->
			</div><!-- end .wrap -->
		</div><!-- end .about-box-bg -->
	</div><!-- end .about-box -->
<?php  ?>
		<?php wp_reset_postdata();
}