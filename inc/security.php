<?php
/**
 * Display events on Front Page
 *
 * @package    DCBA
 * @subpackage FrontPage
 * @version    1.0
 * @copyright  Copyright (c) 2018, HokBay, LLC
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 */

if (!current_user_can('manage_options')) {
	add_action('wp_before_admin_bar_render', 'hokbay_admin_bar_remove_menu', 999);
	//add_action('admin_bar_menu', 'hokbay_admin_bar_remove_node', 999);
}

function hokbay_admin_bar_remove_menu() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu('tribe-events');
	$wp_admin_bar->remove_menu('new-content');
}

//add_filter( 'default_hidden_meta_boxes', 'hokbay_hide_meta_boxes', 10, 2 );
function hokbay_hide_meta_boxes( $hidden, $screen ) {
    return array( 'the_grid_item_formats', 'sidebar-layoutdiv', 'formatdiv', 'pageparentdiv', ''); // get these from the css class
}