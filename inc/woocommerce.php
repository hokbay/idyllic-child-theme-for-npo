<?php
/**
 * extension to Woocommerce
 *
 * @package DCBA
 * @subpackage Users
 * @since 0.3
 * 
 */

add_filter( 'woocommerce_return_to_shop_redirect', 'hokbay_change_return_to_shop_button' );
function hokbay_change_return_to_shop_button() {
	return get_site_url();
}

add_filter( 'woocommerce_gateway_icon', 'remove_icon');
function remove_icon( $icons ) {
    $icons = str_replace( '<img src="' . WC_STRIPE_PLUGIN_URL . '/assets/images/amex.svg" class="stripe-amex-icon stripe-icon" alt="American Express" />', '', $icons );
    $icons = str_replace( '<img src="' . WC_STRIPE_PLUGIN_URL . '/assets/images/diners.svg" class="stripe-diners-icon stripe-icon" alt="Diners" />', '', $icons );
    $icons = str_replace( '<img src="' . WC_STRIPE_PLUGIN_URL . '/assets/images/jcb.svg" class="stripe-jcb-icon stripe-icon" alt="JCB" />', '', $icons );
    return $icons;
}