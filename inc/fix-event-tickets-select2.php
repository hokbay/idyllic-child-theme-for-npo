<?php
/*
Plugin Name: Fix Event Tickets WooCommerce select2
Description: Stop Event Tickets from breaking WooCommerce product pages with an old select2 script
Version: 1.2
Author: WebAware, HokBay
Author URI: https://shop.webaware.com.au/
*/

/*
copyright (c) 2017 WebAware Pty Ltd (email : support@webaware.com.au)
copyright (c) 2018 HokBay, LLC (email : support@hokbay.com)

License: GPLv2 https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html
*/

if (!defined('ABSPATH')) {
	exit;
}


add_action('admin_print_styles-post.php', 'event_tickets_woo_select2_fix', 1);
add_action('admin_print_styles-post-new.php', 'event_tickets_woo_select2_fix', 1);

function event_tickets_woo_select2_fix() {

	// only with WooCommerce and Event Tickets running
	if (!function_exists('WC') || !defined('EVENT_TICKETS_DIR')) {
		return;
	}

	// only on WooCommerce products
	global $typenow;
	if (($typenow !== 'product') && ($typenow !== 'wc_membership_plan')) {
		return;
	}

	wp_dequeue_script('tribe-select2');
	wp_deregister_script('tribe-select2');
	wp_dequeue_style('tribe-select2-css');
	wp_deregister_style('tribe-select2-css');

}