<?php
/**
 * Show front page details.
 * 
 * @package    DCBA
 * @subpackage FrontPage
 * @version    0.2
 * @copyright  Copyright (c) 2015, Idyllic_Plus
 * @copyright  Copyright (c) janhenckens
 * @copyright  Copyright (c) 2018, HokBay, LLC
 * @license    http://www.gnu.org/licenses/gpl-2.0.html
 */

add_action( 'idyllic_client_box', 'hokbay_sponsor_logo_box' );

function hokbay_sponsor_logos($category) {
	$args = array (
		'post_type'             => 'sponsor',
		'post_status'           => 'publish',
		'pagination'            => false,
		'order'                 => 'ASC',
		'orderby'               => 'menu_order',
		'posts_per_page'        => '-1',
		'tax_query'             => array(),
	);

	$args['tax_query'] = array(
		array(
			'taxonomy'  => 'sponsor_categories',
			'field'     => 'slug',
			'terms'     => $category,
		),
	);

	$shame = new Wp_Sponsors_Shame();
	$query = new WP_Query($args);
	$sponsors = '';

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) : $query->the_post();
			$id = get_the_ID();
			get_post_meta( $id, 'wp_sponsors_url', true ) != '' ? $link = get_post_meta( $id, 'wp_sponsors_url', true ) : $link = false;
			$link_target = get_post_meta( $id, 'wp_sponsor_link_behaviour', true );
			$target = ($link_target == 1) ? 'target="_blank"' : '';

			$sponsors.='<li>';
			$sponsors.='<a href='.$link.' '.$target.' >';
			$sponsors.=$shame->getImage($id);
			$sponsors.= '</a></li>';
		endwhile;
	wp_reset_postdata();
	return $sponsors;
	}
}

function hokbay_sponsor_logo_box() {
	$idyllic_settings = idyllic_get_theme_options();
	if($idyllic_settings['idyllic_disable_client_box_a'] != 1 || $idyllic_settings['idyllic_disable_client_box_b'] != 1 || $idyllic_settings['idyllic_disable_client_box_c'] != 1){
		$idyllic_client_box = '';
		$idyllic_client_box .= '<div class="client-content-box"><div class="wrap">';
		/* Platinum Sponsors */
			$idyllic_client_box .= '<div class="client-wrap">';
			if($idyllic_settings['client_image_title_a'] !='' || $idyllic_settings['client_image_description_a'] !='' ){
				$idyllic_client_box .='<div class="box-header">';
			if($idyllic_settings['client_image_title_a'] !=''){
					$idyllic_client_box .='<h2 class="box-title">'. esc_html('Platinum Sponsors').'</h2>';
			}
			if($idyllic_settings['client_image_description_a'] !=''){
					$idyllic_client_box .='<p class="box-sub-title">'. esc_html($idyllic_settings['client_image_description_a']).'</p>';
			}
				$idyllic_client_box .='</div>';
			}
			$idyllic_client_box .='<div class="client-slider"> <ul class="slides">';
			$idyllic_client_box .= hokbay_sponsor_logos('platinum');
			$idyllic_client_box 	.= '</ul>
			</div><!-- end .client-slider -->
			</div><!-- end .client-wrap -->';
			/* Gold Sponsors */
			if($idyllic_settings['idyllic_disable_client_box_a'] !=1){
				$idyllic_client_box 	.= '<div class="client-wrap">';
					if($idyllic_settings['client_image_title_a'] !='' || $idyllic_settings['client_image_description_a'] !='' ){
						$idyllic_client_box .='<div class="box-header">';
						if($idyllic_settings['client_image_title_a'] !=''){
							$idyllic_client_box .='<h2 class="box-title">'. esc_html($idyllic_settings['client_image_title_a']).'</h2>';
						}
						if($idyllic_settings['client_image_description_a'] !=''){
							$idyllic_client_box .='<p class="box-sub-title">'. esc_html($idyllic_settings['client_image_description_a']).'</p>';
						}
						$idyllic_client_box .='</div>';
					}
				$idyllic_client_box .='<div class="client-slider"> <ul class="slides">';
				$idyllic_client_box .= hokbay_sponsor_logos('gold');
				$idyllic_client_box 	.= '</ul>
				</div><!-- end .client-slider -->
				</div><!-- end .client-wrap -->';
			}
			/* Silver Sponsors */
			if($idyllic_settings['idyllic_disable_client_box_b'] !=1){
				$idyllic_client_box 	.= '<div class="client-wrap">';
					if($idyllic_settings['client_image_title_b'] !='' || $idyllic_settings['client_image_description_a'] !='' ){
						$idyllic_client_box .='<div class="box-header">';
						if($idyllic_settings['client_image_title_b'] !=''){
							$idyllic_client_box .='<h2 class="box-title">'. esc_html($idyllic_settings['client_image_title_b']).'</h2>';
						}
						if($idyllic_settings['client_image_description_b'] !=''){
							$idyllic_client_box .='<p class="box-sub-title">'. esc_html($idyllic_settings['client_image_description_b']).'</p>';
						}
						$idyllic_client_box .='</div>';
					}
				$idyllic_client_box .='<div class="client-slider"> <ul class="slides">';
				$idyllic_client_box .= hokbay_sponsor_logos('silver');
				$idyllic_client_box 	.= '</ul>
				</div><!-- end .client-slider -->
				</div><!-- end .client-wrap -->';
			}

			/* Bronze Sponsors */
			if($idyllic_settings['idyllic_disable_client_box_c'] !=1){
				$idyllic_client_box 	.= '<div class="client-wrap">';
					if($idyllic_settings['client_image_title_c'] !='' || $idyllic_settings['client_image_description_a'] !='' ){
						$idyllic_client_box .='<div class="box-header">';
						if($idyllic_settings['client_image_title_c'] !=''){
							$idyllic_client_box .='<h2 class="box-title">'. esc_html($idyllic_settings['client_image_title_c']).'</h2>';
						}
						if($idyllic_settings['client_image_description_c'] !=''){
							$idyllic_client_box .='<p class="box-sub-title">'. esc_html($idyllic_settings['client_image_description_c']).'</p>';
						}
						$idyllic_client_box .='</div>';
					}
				$idyllic_client_box .='<div class="client-slider"> <ul class="slides">';
				$idyllic_client_box .= hokbay_sponsor_logos('bronze');
				$idyllic_client_box 	.= '</ul>
				</div><!-- end .client-slider -->
				</div><!-- end .client-wrap -->';
			}

			$idyllic_client_box 	.= '</div><!-- end .wrap --> </div> <!-- end .client-content-box -->';
			echo $idyllic_client_box;
	}
} 