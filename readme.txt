=== Idyllic Child Theme specifically designed for dcba.com ===

Contributors: 
	Copyright (c) 2018 HokBay, LLC (http://hokbay.com)
	Copyright (c) ? themefreesia
	and those specified in particular files or folders.

Tags: threaded-comments, right-sidebar, four-columns, custom-background, custom-colors, custom-header, custom-logo, custom-menu, editor-style, featured-images, flexible-header, post-formats, footer-widgets, sticky-post, theme-options, translation-ready, e-commerce, education, portfolio

Requires at least: 4.0
Tested up to: 4.8

License: 
	Code created by themefreesia:
		GNU General Public License version 3.0, unless otherwise specified.
		License URI: http://www.gnu.org/licenses/gpl-3.0.html

	Code created by HokBay, LLC:
		HokBay, LLC(HokBay) assign all copyrights to DETROIT CHINESE BUSINESS ASSOCIATION(DCBA), upon complete payment for the project. 
		DCBA is free to use, modify, or distribute code created by HokBay at its own consent.

	Code created by other thrid parties:
		License term specified in corresponding file.

Description: Idyllic Child is a customlized child theme based on Idyllic Theme by themefreesia. Designed for use on dcba.com

=================================================================================